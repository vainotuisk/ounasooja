﻿#   Õunasööja
#   Kursuse 5. kodune programmeerimisülesanne     
#         
#   Väino Tuisk
 # -*- coding: utf-8 -*-
import pygame,sys,random

# konstandid
WIDTH = 800
HEIGHT = 600
ounade_arv = 5
skoor = 0
samm = 5
viivitus = 50
intervall = 20

VALGE = (255,255,255)

pygame.init()
pygame.mixer.init()
ekraan = pygame.display.set_mode([WIDTH, HEIGHT])
pygame.display.set_caption("Õunasööja")
ounake = pygame.image.load("ounake.png") # 38x38
ogija = pygame.image.load("ogija.png") # 107x82
ogija2 = pygame.image.load("ogija2.png")
loputeade = pygame.image.load('lopp.png') # 213x60
ampsuheli = pygame.mixer.Sound("amps.wav")

x = WIDTH / 2
y = HEIGHT / 2
ekraan.fill(VALGE)
ounad = []
eemaldada = []
teksti_font = pygame.font.Font(None, 25)
skooritekst = teksti_font.render("Skoor: ", 1, [0, 0, 0])


def kaugus(punkt1,punkt2):
# punktidevahelise kauguse leidmine
    d = ((punkt1[0] - punkt2[0])**2 + (punkt1[1] - punkt2[1])**2)* 0.5
    return d


for i in range(ounade_arv):
    # õunade paigutamine ekraanile
    ekraan.blit(ounake,(random.randrange(30,WIDTH-30),random.randrange(30,HEIGHT-30)))
    ounad.append([random.randrange(50,WIDTH-50),random.randrange(50,HEIGHT-50)])

pygame.display.flip()   
pygame.key.set_repeat(viivitus,intervall)

 
## siit algab põhiline asi
jookseb = True
while jookseb:

    ekraan.fill(VALGE)
    for i in range(len(ounad)):
        # spraitide kattuvuse kontroll, kui kattub lisatakse eemaldamise järjendisse 
        oun = ounad[i]        
        if kaugus((oun[0] + 19,oun[1] + 19),(x + 53,y + 41)) < 40:
            eemaldada.append(ounad[i])
        ekraan.blit(ounake,oun)
    if len(eemaldada) > 0:
        # eemaldatakse sprait, mis kattus
        ounad.remove(eemaldada[0])
        skoor +=1
        ampsuheli.play()
        eemaldada = []
   
    if (x + y) % 25 > 12:
        # suu animatsioon
        ekraan.blit(ogija, (x  % WIDTH,y % HEIGHT))
    else:
        ekraan.blit(ogija2,(x % WIDTH,y % HEIGHT))
    
    ekraan.blit(skooritekst, [10, 10])
    skoor_ = teksti_font.render(str(skoor),1,[200,0,0])
    ekraan.blit(skoor_,[70,10])
    if skoor == ounade_arv:
    # kui lõpp siis:        
        ekraan.blit(loputeade,(WIDTH/2 - 106,HEIGHT/2 -60))
        for i in pygame.event.get():
        # hiire lugemine    
            if i.type == pygame.MOUSEBUTTONDOWN:
                if i.pos[0] > 296 and i.pos[0] < 503 and i.pos[1] > 240 and i.pos[1] < 300:
                    jookseb = False
    pygame.display.flip()
    # klaviatuurilt nooleklahvide lugemine
    for i in pygame.event.get():
        if i.type == pygame.QUIT:
            sys.exit()        # Kui sündmuseks on klahvi allavajutamine...
        elif i.type == pygame.KEYDOWN: 
            # ... ja klahviks on nooleklahv üles liikumiseks,...
            if i.key == pygame.K_UP: 
                # ... siis vähendame y-koordinaati
                y = y - samm 
            elif i.key == pygame.K_DOWN:
                y = y + samm
            elif i.key == pygame.K_LEFT:
                x = x - samm
            elif i.key == pygame.K_RIGHT:
                x = x + samm
            elif i.key == pygame.K_x:
                sys.exit()
   
pygame.quit()
