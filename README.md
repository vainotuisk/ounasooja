# Tavapärane README

__Koolituse ülesanne ÕUNASÖÖJA__


Koostage programm, mis joonistab programmiakna koos vähemalt viie õunaga ning liikuva tegelasega, kes armastab õunu süüa. Õunteks ja sööjaks võite kasutada nii vastavaid pilte kui lihtsalt kokkuleppelisi kujundeid (ringid vms). Pange sööja liikuma vastavalt klaviatuuri nooleklahvide vajutusele (üles, alla, vasakule, paremale, näited olemas käsiraamatus). Lisage "söömiskontroll": kui sööja liigub õuna peale, juhtub õunaga midagi (õunapilt kaob või muutub teistsuguseks, sööja saab punkti vms) (ehk kontrollitakse, kas sööja koordinaadid ühtivad mõne õuna koordinaatidega). Kuvage ekraani nurgas ka jooksev punktiseis.

Suhtuge ülesandesse vabalt: lisage veel funktsionaalsust, helisid jms. Kui mõni osa ei tööta, küsige või kommenteerige vigased koodiosad välja ja lisage omapoolsete kommentaarid (mida püüdsite, millise vea saite).

Kodune töö (nii programmifail kui abifailid, et kõik, mida programmi tööks vaja, oleks olemas, näiteks kokkupakituna zip- või rar-failiks) laadige Moodle'isse.

__Versioon 1__

__Vead:__

* kattumise asukoht on praegu spraitide keskpunkt, peaks/võiks olla rohkem suu asukohas
* kala koordinaadid, peaks panema spraidi keskkohta - praegu on vasakul üleval nurgas nagu tavaliselt, sellest tuleneb kala ebanormaalne köitumine üle ääre liikumisel


Tegemise käigus tuli veel ideid, mida võib edaspidi realiseerida: 

* parem animatsioon
* uue mängu alustamise võimalus mängu seest, lõpumenüüst
* õunade animeerimine
* lisada haikala(d), mis võtavad kalaga puutudes punkti


Vt [https://bitbucket.org/vainotuisk/ounasooja](https://bitbucket.org/vainotuisk/ounasooja)
